// console.log("Hello World");

// [Section] Assignment Operator
		// Basic Assignment Operator (=)
		//  The assignment operator assignes the value of the right hand operand to a variable.
		let assignmentNumber = 8;
		console.log("The value of the assignmentNumber variable: " + assignmentNumber);
		console.log("----------");


// [Section] Arithmetic Operations

	let	x = 200;
	let y = 18;

	console.log("x: " + x);
	console.log("y: " + y);
	console.log("----------");


	// Addition 
	let sum = x + y;
	console.log("Result of addition operator: " + sum);


	// Subtraction
	let difference = x - y;
	console.log("Result of subtraction operator: " + difference);


	//Multiplication
	let product = x * y;
	console.log("Result of multiplication operator: " + product);

	//Division
	let quotient = x / y;
	console.log("Result of divsion operator: " + quotient);

	// Remainder
	let modulo = x % y;
	console.log("Result of modulo operator: " + modulo);


// Continuation of assignment operator

// Addition Assignment Operator
	//Syntax: 

	// long method (addition)
	//  current value of assignmentNumber is 8, and is changed to assignmentNumber + 2 (10).
	// assignmentNumber = assignmentNumber + 2; // long method
	// console.log(assignmentNumber);


	// short method (addition)
	assignmentNumber += 2;
	console.log("Result of Addition Assignment Operator: " +assignmentNumber); // assignmentNumber value : 10 

	// long method (subtraction)
	// assignmentNumber = assignmentNumber - 3;
	// console.log(assignmentNumber); // result : 10 - 3 = 7

	// short method (subtraction)
	assignmentNumber -= 3;
	console.log("Result of Subtraction Assignment Operator: " + assignmentNumber); // result : 10 - 3 = 7   


	// long method (multiplication)
	//	assignmentNumber = assignmentNumber * 2;
	// console.log(assignmentNumber) // result : 7 * 2 = 14

	// short method (multiplication)
	assignmentNumber *= 2;
	console.log("Result of Multiplication Assignment Operator: " + assignmentNumber); // result : 7 * 2 = 14

	// long method (division)
	//	assignmentNumber = assignmentNumber / 3;
	// console.log(assignmentNumber) // result : 7 / 2 = 2

	// short method (division)
	assignmentNumber /= 2;
	console.log("Result of division Assignment Operator: " + assignmentNumber); // result : 7 / 2 = 2

// [Section] PEMDAS (Order of Operations)
// Multiple Operators and Parenthesis

	let mdas = 1 + 2 - 3 * 4 / 5; 
	console.log("Result of mdas operation: " + mdas);
		/*
			The operations were done in the following order:
			1. 3 * 4 = 12 , 1 + 2 - 12 / 5 
			2. 12 / 5 = 2.4 , 1 + 2 - 2.4 
			3. 1 + 2  = 3 , 3 - 2.4
			4  3 - 2.4 = 0.6
		*/
	let pemdas = (1+(2-3)) * (4/5);
	console.log("Result of pemdas operation: " + pemdas);

// heirarchy
// combinations of multiple arithmetic operators will follow the pemdas rule

	/* 
		1. parenthesis
		2. exponent
		3. multiplication or division
		4. addition or subtraction

		Note: will also follow left to right rule

	*/

// [Section] Increment (add) and Decrement (substract / lessen)

// Operators that add or subtract values by 1 and reassign the value of the variable where the increment(++)/decrement(--) was applied.

let z = 1; 

//pre-increment (takes effect instantly)

let increment = ++z;
console.log("Result of pre-increment: " + increment); // 2
console.log("Result of pre-increment of z: " + z); // 2 

// post-increment (takes effect after)
increment = z++;
console.log("Result of post-increment: " + increment); //2
console.log("Result of post-increment of z: " + z); // "3"

/* increment = z++;
console.log("Result of post-increment: " + increment);
console.log("Result of post-increment of z: " + z);

*/

// pre decrement
let decrement = --z;
console.log("Result of pre-decrement: " + decrement); //2
console.log("Result of pre-decrement of z: " + z); //2

// post-decrement

decrement = z--;
console.log("Result of pre-decrement: " + decrement); //2
console.log("Result of pre-decrement of z: " + z); // "1"


// [Section] Type Coercion
//  is the automatic conversion of values from one data type to another.

	// combination of number and string data type will result to string.
	let numA = 10; // number 
	let numB = "12"; // string

	let coercion = numA + numB; 
	console.log(coercion); //1012 (10 and 12 combined not added)
	console.log(typeof coercion);

	// combination of number and number data type will result to number.
	let numC = 16;
	let numD = 14;
	let nonCoercion = numC + numD;
	console.log(nonCoercion);

	// combination of number and boolean data type will result to number 
	let numX = 10;
	let numY = true;
			// boolean values are:
			// true = 1;
			// false = 0;

	coercion = numX + numY;
	console.log(coercion);
	console.log(typeof coercion);

	let num1 = 1;
	let	num2 = false;
	coercion = num1 + num2;
	console.log(coercion);
	console.log(typeof coercion);

// [SECTION] Comparison Operators
// Comparison operators are used to evaluate and compare the left and right operands
// After evaluation, it returns a boolean value

// equality operator ( == -> read as 'equal to')
// compares the value, but not the data type
console.log("----------");
console.log(1 == 1); //true
console.log(1 == 2); //false
console.log(1 == '1'); //true
console.log(0 == false); //true - 'false' boolean value is also equal to zero.

let juan = "juan";
console.log('juan' == 'juan'); //true
console.log('juan' == 'Juan'); //false // equality operator is strict with letter casing.
console.log(juan == "juan"); //true

// inequality operator <!= - also read as 'not equal'>
console.log("----------");
console.log(1 != 1); //false
console.log(1 != 2); //true
console.log(1 != '1'); // false
console.log(0 != false); // false
console.log('juan' != 'juan'); //false
console.log('juan' != 'Juan'); //true
console.log(juan != "juan"); //false

// [Section] Relational Operators
// Some comparison operators to check wether one value is greater or less than the other value.
// It will return a value of true or false

console.log("----------");

let a = 50;
let b = 65;

// GT or Greater Than Operator(>)
let isGreaterThan = a > b; // 50 > 65 = false
console.log(isGreaterThan);


// LT or Less Than Operator(<)
let isLessThan = a < b; // 50 < 65 = true
console.log(isLessThan);

// GTE or Greater Than or Equal (>=)
let isGTorEqual = a >= b; //false
console.log(isGTorEqual);

// LTE or Less Than or Equal (<=)
let isLTorEqual = a <= b; //true
console.log(isLTorEqual);

// Forced Coercion
let num = 56;
let numStr = "30";
console.log(num > numStr); //true

let str = "twenty";
console.log(num >= str); //false
// Since the string is not numeric, the string will not be converted to a number. 

// Logical AND operator (&&)
console.log("----------");
// Logical OR operator
let isLegalAge = true;
let isRegistered = false;

let allRequirementsMet = isLegalAge && isRegistered; //false
console.log("Result of logical AND operator: " + allRequirementsMet);
// And operator requires all / both are true


// Logical OR Operator (||)
let someRequirementsMet = isLegalAge || isRegistered;
console.log("Result of logical OR operator: " + someRequirementsMet);
// OR operator requires only 1 true;


// Logical Not Operator (!)
// Returns the opposite value
let someRequirementsNotMet = !isLegalAge;
console.log("Result of logical NOT operator: " + someRequirementsNotMet); //false








